import { Component, OnInit } from '@angular/core';
import {ChatserviceService} from '.././chatservice.service';
@Component({
  selector: 'app-chatcomponent',
  templateUrl: './chatcomponent.component.html',
  styleUrls: ['./chatcomponent.component.css']
})
export class ChatcomponentComponent implements OnInit {
  user;
  chatData  =  [];
  constructor(private _ChatserviceService: ChatserviceService) { }

  ngOnInit() {
    this.user = this._ChatserviceService.username;
    this._ChatserviceService.fetchChatData().subscribe(response => this.chatData = response);

  }

}
