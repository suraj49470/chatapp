import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';
import {ChatserviceService} from './chatservice.service';
import { AppComponent } from './app.component';
import { ChatcomponentComponent } from './chatcomponent/chatcomponent.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatcomponentComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    ChatserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
