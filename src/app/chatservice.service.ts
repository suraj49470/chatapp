import { Injectable } from '@angular/core';
import {Http , Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ChatserviceService {
  constructor(private _http: Http) { }
   username = 'suraj singh';
   userObj = [
     {
       id : 1,
       name : 'suraj singh'
     },
     {
       id : 2,
       name : 'rohan singh'
     }
   ];

   getUserObj() {
          return this.userObj;
   }

   fetchChatData() {
           return this._http.get('https://jsonplaceholder.typicode.com/posts')
             .map((response: Response) => response.json());
   }

}
