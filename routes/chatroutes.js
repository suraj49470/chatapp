

var express  = require('express');
var router  = express.Router();
var chatModel = require('.././models/chat');
router.route('/')
  .post(function (req,res) {

          //  populate user schema
             var newChat = new chatModel.chatModel({
                    uname : req.body.uname,
                    messages : [
                      {
                        by : req.body.by,
                        message : req.body.message
                      }
                    ]
             });

           // look for user
            chatModel.userChatModel.findOne({uname:req.body.uname},function (err,userfound) {
              if(err){
                throw err;
              }
              if(userfound){ //if found update record

                var newmessage = {"by":req.body.by,"message":req.body.message};

                chatModel.updateUserModel.update({ _id: userfound._id },
                  {
                    $push: {messages:newmessage}
                  }
                  , { upsert: true }, function(userupdateerr,userupdateddco) {
                 if(userupdateerr){
                   console.log(userupdateerr);
                 }

                 res.send({"status":"success","message":"message sent","response":userupdateddco});

                });



              }else{         // insert new user

                newChat.save(function (newusererr,newuserdoc) {
                  if(newusererr){
                    throw newusererr;
                  }
                  res.send({"status":200,"message":"success","response":newuserdoc});
                });

              }

            });

  });

router.route('/getuserchat')
  .post(function (req,res) {

       chatModel.userChatModel.findOne({uname:req.body.uname},function (err,doc) {
                      if(err){
                        throw err;
                      }
                              res.send({"status":"success","message":"user chats recieved","response":doc});
         });

  });

module.exports = router;
