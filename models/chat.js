var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/chat',{"useMongoClient":true});
var  Schema  =  mongoose.Schema;

var chatSchema = new Schema({

          uname : {type:String},
          messages : [
            {
              by: { type:String},
              message: {type:String},
              _id:false
            }

          ]
});

var userChatSchema = new Schema({
        uname : {type:String}
});

var updateUserShema = new Schema({
  messages : [
    {
      by: { type:String},
      message: {type:String},
      _id:false
    }

  ]
});

var chatModel = mongoose.model('chatModel',chatSchema,"chats");
var userChatModel = mongoose.model('userChatModel',userChatSchema,"chats");
var updateUserModel = mongoose.model('updateUserModel',updateUserShema,"chats");
module.exports = {chatModel:chatModel,userChatModel:userChatModel,updateUserModel:updateUserModel};


